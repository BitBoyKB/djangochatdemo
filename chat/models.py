from django.db import models

# Create your models here.
class Message(models.Model):
    """
    Model for messages
    """

    #Fields
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    message = models.TextField(max_length=3000)
    message_html = models.TextField()
    createDate = models.DateTimeField(auto_now_add=True)
    upDate = models.DateTimeField(auto_now=True)

    def __str__(self):
        """
        String for the message
        """

        return self.message
